/*
 * Chocolate.h
 *
 *  Created on: 19-Mar-2021
 *      Author: ridit
 */

#ifndef CHOCOLATE_H_
#define CHOCOLATE_H_

#include "Food.h"



class Chocolate: public Food {
public:
	Chocolate();
	virtual ~Chocolate();
	void displayType();
};



#endif /* CHOCOLATE_H_ */
