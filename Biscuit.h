/*
 * Biscuit.h
 *
 *  Created on: 17-Mar-2021
 *      Author: ridit
 */

#ifndef BISCUIT_H_
#define BISCUIT_H_

#include "Food.h"



class Biscuit: public Food {
public:
	Biscuit();
	virtual ~Biscuit();
	void displayType();
};


#endif /* BISCUIT_H_ */
