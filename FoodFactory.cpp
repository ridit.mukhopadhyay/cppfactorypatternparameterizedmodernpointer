/*
 * FoodFactory.cpp
 *
 *  Created on: 17-Mar-2021
 *      Author: ridit
 */

#include "FoodFactory.h"



FoodFactory::FoodFactory() {


}

FoodFactory::~FoodFactory() {

}
unique_ptr<Food> FoodFactory::makeFood(string foodType){

	unique_ptr<Food> retVal = make_unique<Food>();

	if(foodType == "ch"){
		 //product = make_unique<Chocolate>();
		retVal = make_unique<Chocolate>();
	}
	if(foodType == "bi"){
		//product = make_unique<Biscuit>();
		retVal = make_unique<Biscuit>();

	}

	//return move(product);
	return retVal;
}



