/*
 * FoodFactory.h
 *
 *  Created on: 17-Mar-2021
 *      Author: ridit
 */

#ifndef FOODFACTORY_H_
#define FOODFACTORY_H_

#include <memory>
#include <iostream>
#include <string>
#include "Chocolate.h"
#include "Biscuit.h"
#include "Food.h"
using namespace std;



class FoodFactory {

public:
	FoodFactory();
	virtual ~FoodFactory() ;
	static unique_ptr<Food> makeFood(string foodType);
};



#endif /* FOODFACTORY_H_ */
