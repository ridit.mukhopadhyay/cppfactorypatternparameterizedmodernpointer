//============================================================================
// Name        : FactoryPattern3.cpp
// Author      : Ridit
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <iostream>
#include "FoodFactory.h"
using namespace std;

int main() {


	unique_ptr<Food> ch  = FoodFactory:: makeFood("ch");

	unique_ptr<Food> bi = FoodFactory:: makeFood("bi");

	ch->displayType();

	bi->displayType();


	return 0;
}
