/*
 * Food.h
 *
 *  Created on: 19-Mar-2021
 *      Author: ridit
 */

#ifndef FOOD_H_
#define FOOD_H_

#include <iostream>
#include <string>

#include <typeinfo>

using namespace std;



class Food {
public:
	Food();
	virtual ~Food();
	virtual void displayType();

};



#endif /* FOOD_H_ */
